const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const productController = require("../Controllers/productController");

router.post("/add", auth.verify, productController.addProduct);
router.get("/view", productController.viewProduct);

module.exports = router;