const Product = require("../Models/productsSchema.js");
const auth = require("../auth.js");
const mongoose = require("mongoose");

module.exports.addProduct = (request, response) =>{
	let input = request.body;
	const userData = auth.decode(request.headers.authorization);

		if(userData.isAdmin) {let newProduct = new Product({
			name: input.name,
			description: input.description,
			price: input.price
		});
	
		return newProduct.save()
		.then(product =>{
			console.log(product);
			response.send(product);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		})}
		else {
			return response.send("You are not an admin.");
		}
}

module.exports.viewProduct = (request, response) => {
	const product = request.body;
	let isActive = product.isActive;

	Product.find({isActive: true})
	.then(result =>{
		return response.send(result);
	})
	.catch(error =>{
		console.log(error);
		response.send(false);
	})
}